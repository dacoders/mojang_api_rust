use validator::{ValidationErrors, ValidationErrorsKind};

use super::api_error::{ApiError, FieldError};

pub fn validation_errors_to_api_error(errors: ValidationErrors) -> ApiError {
    ApiError::ValidationError(validation_errors_to_list(errors, None))
}

//TODO: In order to ensure readability, only the first error is printed, this may need to be expanded on in the future!
pub fn validation_errors_to_list(
    errors: ValidationErrors,
    adder: Option<String>,
) -> Vec<FieldError> {
    let map = errors.into_errors();

    return map
        .iter()
        .map(|(field, error)| match error {
            ValidationErrorsKind::Struct(errors) => {
                validation_errors_to_list(*errors.clone(), Some(format!("of item {}", field)))
            }
            ValidationErrorsKind::List(list) => list
                .values()
                .enumerate()
                .map(|(index, errors)| {
                    validation_errors_to_list(
                        *errors.clone(),
                        Some(format!("of list {} with index {}", field, index)),
                    )
                })
                .flatten()
                .collect(),
            ValidationErrorsKind::Field(errors) => errors
                .iter()
                .map(|error| {
                    if let Some(adder) = &adder {
                        FieldError {
                            field: format!("{} {}", field, adder),
                            error: error.message.as_ref().unwrap_or(&error.code).to_string(),
                        }
                    } else {
                        FieldError {
                            field: field.to_string(),
                            error: error.message.as_ref().unwrap_or(&error.code).to_string(),
                        }
                    }
                })
                .collect(),
        })
        .flatten()
        .collect();
}
