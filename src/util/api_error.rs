#![allow(dead_code)]
#![allow(clippy::enum_variant_names)]

use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum DatabaseError {
    #[error("Error while interacting with the database: {0}")]
    SqlxError(#[from] sqlx::error::Error),
    #[error("Error while trying to generate an ID")]
    IdGenerationError,
    #[error("A database request failed")]
    Other(String),
}

/// An error returned by the API
#[derive(Serialize, Deserialize)]
pub struct ApiErrorStruct {
    pub error: String,
    pub description: String,
    pub data: Option<Value>,
}

#[derive(Error, Debug)]
pub enum AuthenticationError {
    #[error("Environment Error")]
    EnvError(#[from] dotenvy::Error),
    #[error("An unknown database error occurred: {0}")]
    SqlxError(#[from] sqlx::Error),
    #[error("An unknown database error occurred: {0}")]
    DatabaseError(#[from] DatabaseError),
    #[error("Error while parsing JSON: {0}")]
    SerdeError(#[from] serde_json::Error),
    #[error("Invalid Authentication Credentials")]
    InvalidCredentialsError,
    #[error("Invalid E-Mail Confirmation")]
    InvalidEmailConfirmError,
    #[error("Invalid JWT time")]
    JWTTimeError,
    #[error("An unknown authentication error has occured")]
    UnknownAuthenticationError,
    #[error("An unknown Argon2 error has occured")]
    Argon2Error(#[from] argon2::Error),
    #[error("Authentication Error: {0}")]
    Other(String),
}

/// A struct representing an error for a field.
#[derive(Debug, Serialize, Deserialize)]
pub struct FieldError {
    pub field: String,
    pub error: String,
}

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Environment Error")]
    EnvError(#[from] dotenvy::Error),
    #[error("An unknown database error occurred: {0}")]
    DatabaseError(#[from] DatabaseError),
    #[error("An unknown database error occurred: {0}")]
    SqlxError(#[from] sqlx::Error),
    #[error("Deserialization error: {0}")]
    JsonError(#[from] serde_json::Error),
    #[error("Authentication Error: {0}")]
    AuthenticationError(#[from] AuthenticationError),
    #[error("Invalid Input: {0}")]
    InvalidInputError(String),
    #[error("Error while validating input")]
    ValidationError(Vec<FieldError>),
    #[error(transparent)]
    Other(#[from] anyhow::Error),
    #[error("{0}")]
    Unique(String),
}

impl ApiError {
    pub fn error_struct(&self) -> ApiErrorStruct {
        ApiErrorStruct {
            error: match self {
                ApiError::EnvError(..) => "environment_error",
                ApiError::SqlxError(..) => "database_error",
                ApiError::DatabaseError(..) => "database_error",
                ApiError::AuthenticationError(..) => "unauthorized",
                ApiError::JsonError(..) => "json_error",
                ApiError::InvalidInputError(..) => "invalid_input",
                ApiError::ValidationError(..) => "invalid_input",
                ApiError::Other(..) => "other",
                ApiError::Unique(s) => s.as_str(),
            }
            .to_string(),
            description: self.to_string(),
            data: match self {
                ApiError::ValidationError(errors) => Some(json!(errors)),
                _ => None,
            },
        }
    }
}

impl actix_web::ResponseError for ApiError {
    fn status_code(&self) -> actix_web::http::StatusCode {
        match self {
            ApiError::EnvError(..) => actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            ApiError::DatabaseError(..) => actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            ApiError::SqlxError(..) => actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            ApiError::AuthenticationError(..) => actix_web::http::StatusCode::UNAUTHORIZED,
            ApiError::JsonError(..) => actix_web::http::StatusCode::BAD_REQUEST,
            ApiError::InvalidInputError(..) => actix_web::http::StatusCode::BAD_REQUEST,
            ApiError::ValidationError(..) => actix_web::http::StatusCode::BAD_REQUEST,
            ApiError::Other(..) => actix_web::http::StatusCode::BAD_REQUEST,
            ApiError::Unique(..) => actix_web::http::StatusCode::BAD_REQUEST,
        }
    }

    fn error_response(&self) -> actix_web::HttpResponse {
        actix_web::HttpResponse::build(self.status_code()).json(self.error_struct())
    }
}
