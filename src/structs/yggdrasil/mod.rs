//! https://wiki.vg/Authentication
//! https://wiki.vg/Authentication#Authentication_Error
use serde::Serialize;

/// A struct representing an error.
#[derive(Debug, Serialize, Default)]
pub struct YggdrasilError {
    pub error: String,
    #[serde(rename = "errorMessage")]
    pub error_message: String,
    pub cause: Option<String>,
}
