use actix_web::{get, web, HttpResponse, Scope};
use serde_json::json;

pub mod authenticate;
pub mod refresh;
pub mod validate;

#[get("")]
async fn route_index_get() -> HttpResponse {
    HttpResponse::Ok().json(json!({
        "message": "Please check the docs :)"
    }))
}

pub fn scope() -> Scope {
    web::scope("/authserver").service(route_index_get)
}
