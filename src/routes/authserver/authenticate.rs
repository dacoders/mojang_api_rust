//! Route for authenticating users using their passwords.
//! https://wiki.vg/Authentication#Authenticate

use serde::Deserialize;

#[derive(Deserialize)]
enum MojangGame {
    Minecraft,
    Scrolls,
}

#[derive(Deserialize)]
struct AuthenticatePayloadAgent {
    pub name: MojangGame,
    pub version: f32,
}
impl Default for AuthenticatePayloadAgent {
    fn default() -> Self {
        Self {
            name: MojangGame::Minecraft,
            version: 1.0,
        }
    }
}

#[derive(Deserialize)]
struct AuthenticatePayload {
    #[serde(default)]
    pub agent: AuthenticatePayloadAgent,
    /// Can be an email address or player
    /// name for unmigrated accounts.
    pub username: String,
    pub password: String,
    #[serde(rename = "clientToken")]
    pub client_token: Option<String>,
    /// optional; default: false; true adds the user object to the response
    #[serde(default)]
    pub requestUser: bool,
}
