use actix_web::{get, HttpResponse};
use serde::Deserialize;

#[derive(Deserialize)]
struct ValidatePayload {
    #[serde(rename = "accessToken")]
    pub access_token: String,
    #[serde(rename = "clientToken")]
    pub client_token: Option<String>,
}

#[get("/validate")]
async fn route_index_get() -> HttpResponse {
    HttpResponse::NoContent().finish()
}
