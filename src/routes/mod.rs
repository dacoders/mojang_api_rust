use actix_web::{get, web, HttpResponse, Responder};
use serde_json::json;

pub mod authserver;
pub mod mojang_api;

#[get("/health")]
async fn route_health_get() -> impl Responder {
    "OK".to_string()
}

#[get("/")]
async fn route_index_get() -> HttpResponse {
    HttpResponse::Ok().json(json!({
        "status": "ok",
        "name": "mojang_api_rust",
        "version": env!("CARGO_PKG_VERSION"),
        "message": "Welcome to the API!"
    }))
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(route_index_get)
        .service(route_health_get)
        .service(mojang_api::scope())
        .service(authserver::scope());
}
