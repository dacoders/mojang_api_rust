use log::info;
use sqlx::migrate::{Migrate, MigrateDatabase};
use sqlx::postgres::{PgPool, PgPoolOptions};
use sqlx::{Connection, PgConnection, Postgres};

pub async fn connect() -> Result<PgPool, sqlx::Error> {
    info!("Initializing database connection");
    let database_url = dotenvy::var("DATABASE_URL").expect("`DATABASE_URL` does not exist");
    let pool = PgPoolOptions::new()
        .min_connections(
            dotenvy::var("DATABASE_MIN_CONNECTIONS")
                .ok()
                .and_then(|x| x.parse().ok())
                .unwrap_or(1),
        )
        .max_connections(
            dotenvy::var("DATABASE_MAX_CONNECTIONS")
                .ok()
                .and_then(|x| x.parse().ok())
                .unwrap_or(16),
        )
        .connect(&database_url)
        .await?;

    Ok(pool)
}
pub async fn check_for_migrations() -> Result<(), sqlx::Error> {
    let uri = dotenvy::var("DATABASE_URL").expect("`DATABASE_URL` does not exist");
    let uri = uri.as_str();
    if !Postgres::database_exists(uri).await? {
        info!("Creating database");
        Postgres::create_database(uri).await?;
    }
    info!("Applying migrations");
    run_migrations(uri).await?;

    Ok(())
}

// pub const MIGRATION_FOLDER: &str = "./migrations";

pub async fn run_migrations(uri: &str) -> Result<(), sqlx::Error> {
    let mut conn: PgConnection = PgConnection::connect(uri).await?;

    conn.ensure_migrations_table().await?;

    sqlx::migrate!("./migrations").run(&mut conn).await?;

    Ok(())
}

pub mod models;
