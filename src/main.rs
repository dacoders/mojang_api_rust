use std::path::Path;

use actix_web::{middleware::Logger, App, HttpServer};
use anyhow::Context;
use log::info;

use crate::util::db_pool::DATABASE_POOL;

pub mod database;
pub mod routes;
pub mod structs;
pub mod util;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    if !Path::new(".env").exists() {
        std::fs::copy("default.env", ".env")
            .context("Failed to copy default.env to .env")
            .unwrap();
        println!("Automatically copied default.env to .env");
    }
    dotenvy::dotenv().ok();
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));
    info!("Starting mojang_api_rust");

    database::check_for_migrations()
        .await
        .expect("An error occurred while running migrations.");

    // Database Connector
    let pool = database::connect()
        .await
        .expect("Database connection failed");

    DATABASE_POOL
        .set(pool.clone())
        .unwrap_or_else(|_| panic!("Failed to set the database global"));

    info!("Starting HTTP server");

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::new(
                r#"%a "%r" %s %bB "%{Referer}i" "%{User-Agent}i" %D"#,
            ))
            .configure(routes::config)
    })
    .bind(dotenvy::var("BIND_ADDR").unwrap())?
    .run()
    .await
    .map_err(|e| e.into())
}
